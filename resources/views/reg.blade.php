<x-header />

<h1>user registration</h1>

<form action="userReg" method="post">
    @csrf
    <input type="text" name="name" placeholder="Enter Your Name">
    <span style="color: red">@error('name')
    {{$message}}
    @enderror</span>
    <br><br>
    <input type="text" name="fname" placeholder="enter your father name">
    <span style="color: red">@error('fname')
    {{$message}}
    @enderror</span>
    <br><br>
    <input type="text" name="email" placeholder="enter your email">
    <span style="color: red">@error('email')
    {{$message}}
    @enderror</span>
    <br><br>
    <input type="number" name="number" placeholder="enter your phone number">
    <span style="color: red">@error('number')
    {{$message}}
    @enderror</span>
    <br><br>
    <button type="submit" >Register</button>
</form>
<x-footer />
