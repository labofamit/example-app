<x-header />
<h1>login</h1>
<form action="loginUser" method="POST">
    @csrf
    <input type="email" name="email" >
    <span style="color:red">@error('email'){{$message}}@enderror</span>
    <br><br>
    <input type="password" name="password">
    <span style="color: red">
    @error('password')
        {{$message}}
    @enderror</span>
    <br><br>
    <button type="submit">Login</button>
</form>
<x-footer />
