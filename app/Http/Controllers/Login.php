<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Login extends Controller
{
    function loginUser(Request $req){
        $req->validate(
            ["email"=>"required |min:10 |max:100",
            "password"=>"required |min:5"
            ]);
    return $req->input();
    }
}
