<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class usersController extends Controller
{
    //
    function myUser()
    {
        $response=Http::get('https://reqres.in/api/user');

        return view("users",['responselist'=>$response['data']]);
    }
}
