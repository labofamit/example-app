<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\usersController;
use App\Http\Controllers\Login;
use App\Http\Controllers\Dashbord;
use App\Http\Controllers\userRegistration;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//controller calling

Route::get('users',[usersController::class,'myUser']);

Route::post('loginUser',[Login::class,'loginUser']);

Route::post('userReg',[userRegistration::class,'getData']);


//controller calling with paramiter
Route::get('dashbord/{id}',[Dashbord::class,'userDashbord']);


         //view calling
Route :: get('/',function(){
    return view('home');
});

Route::get('/home', function () {
   return redirect ('');
});

Route::get('/header', function(){
    return view('header');
});

Route::get('/about',function(){
return view('about');
});

// Route::get('/contact', function(){
//     return view('contact');
// });

// or new methods

  Route::view('contact','contact');

 Route::view('header','components\header');

Route::view('/reg', 'reg');
Route::view('/login', 'login');


//// group middle ware

Route::group(['middleware' =>['CountryCheck']],function(){
    //here apply group middle ware on routes
 Route::view('contact','contact');
 Route::view('header','components\header');
//  Route::view('/reg', 'reg');
 Route::view('/login', 'login');
});

Route::view('invalideCountry', 'invalideCountry');
